pointerout

Test Description: This test checks if pointerout event triggers after pointercancel. Start touch on the black rectangle and move your touch to scroll in any direction.

Note: this test is for touch devices only

Pointer Events pointerout tests

The following pointer types were detected: (none).


FAIL pointerout event received assert_true: pointerout should be received before the test finishes expected true got false

