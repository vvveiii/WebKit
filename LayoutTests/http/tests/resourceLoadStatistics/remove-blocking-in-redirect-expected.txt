Tests that blocking is removed mid-flight in redirects.

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS successfullyParsed is true

TEST COMPLETE
  

--------
Frame: '<!--frame1-->'
--------
Set cookie.


--------
Frame: '<!--frame2-->'
--------
Should receive first-party cookie.
Received cookie named 'firstPartyCookie'.
Did not receive cookie named 'partitionedCookie'.
Client-side document.cookie: firstPartyCookie=value

--------
Frame: '<!--frame3-->'
--------
Redirect case 1, should receive first-party cookie for 127.0.0.1.
Received cookie named 'firstPartyCookie'.
Did not receive cookie named 'partitionedCookie'.
Client-side document.cookie: firstPartyCookie=127.0.0.1

--------
Frame: '<!--frame4-->'
--------
Try to set third-party cookie in blocked mode.


--------
Frame: '<!--frame5-->'
--------
Should receive no cookie.
Did not receive cookie named 'firstPartyCookie'.
Did not receive cookie named 'partitionedCookie'.
Client-side document.cookie:

--------
Frame: '<!--frame6-->'
--------
Redirect case 2, should receive first-party cookie for 127.0.0.1.
Received cookie named 'firstPartyCookie'.
Did not receive cookie named 'partitionedCookie'.
Client-side document.cookie: firstPartyCookie=127.0.0.1
